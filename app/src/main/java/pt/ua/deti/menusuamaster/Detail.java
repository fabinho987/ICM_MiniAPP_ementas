package pt.ua.deti.menusuamaster;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Detail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        TextView textType = (TextView) findViewById(R.id.almocoJantar);
        textType.setText("Almoço");
        TextView textDia = (TextView) findViewById(R.id.showdia);
        textDia.setText("Wed, 4 Oct 2017");
        TextView textSopa = (TextView) findViewById(R.id.sopadisplay);
        textSopa.setText("Sopa de espinafres");
        TextView textP1 = (TextView) findViewById(R.id.prato1);
        textP1.setText("Carne de porco a alentejana");
        TextView textP2 = (TextView) findViewById(R.id.prato2);
        textP2.setText("Solha frita com macedonia de legumes");


    }
}
