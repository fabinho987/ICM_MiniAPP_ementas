package pt.ua.deti.menusuamaster;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by fabio on 20/09/17.
 */

public class Ementa implements Serializable {

    private static final long serialVersionUID = -1213949467658913456L;
    private Date date;
    private String canteenSite;
    private String dailyMeal;
    private boolean available;
    private List<String> mealCourseList;
    private String day;

    public Ementa(Date date, String canteenSite, String dailyMeal, boolean available, String day){
        this.date=date;
        this.canteenSite=canteenSite;
        this.dailyMeal=dailyMeal;
        this.available=available;
        this.day=day;
        mealCourseList = new ArrayList<>();
    }

    public Date getDate() {
        return date;
    }

    public String getDay() {
        return day;
    }

    public static ArrayList<Ementa> getItems() {
        ArrayList<Ementa> items = new ArrayList<Ementa>();
        items.add(new Ementa(new Date(),"Crasto","Carne",true,"Monday"));
        items.add(new Ementa(new Date(),"Santiago","Carne",true,"Thursday"));
        items.add(new Ementa(new Date(),"BAR","Carne",true,"Friday"));
        return items;
    }
}