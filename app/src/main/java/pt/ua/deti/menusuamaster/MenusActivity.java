package pt.ua.deti.menusuamaster;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MenusActivity extends AppCompatActivity {

   ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menus);

        Resources res = getResources();

        listView = (ListView) findViewById(R.id.listVw);

        Map<String, String> map = new HashMap<>();
        map.put("Refeitorio de Santiago - Almoço - ","Wed, 4 Oct 2017");
        map.put("Refeitorio de Crasto - Almoço - ","Wed, 4 Oct 2017");
        map.put("Refeitorio de Bar - Almoço - ","Wed, 4 Oct 2017");
        map.put("Refeitorio de Santiago - Jantar - ","Wed, 4 Oct 2017");
        map.put("Refeitorio de Crasto - Jantar - ","Wed, 4 Oct 20177");
        map.put("Refeitorio de Bar - Jantar - ","Wed, 4 Oct 2017");

        final List<HashMap<String,String>> listItems = new ArrayList<>();
        SimpleAdapter adapter = new SimpleAdapter(this,listItems,R.layout.list_item,
                new String[]{"First Line","Second Line"}, new int[]{R.id.txt, R.id.txt1});

        Iterator it = map.entrySet().iterator();
        while (it.hasNext()){
            HashMap<String,String> resulmap = new HashMap<>();
            Map.Entry pair = (Map.Entry)it.next();
            resulmap.put("First Line", pair.getKey().toString());
            resulmap.put("Second Line", pair.getValue().toString());
            listItems.add(resulmap);
        }

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                    switch (position%2){
                    case 0 :
                        Intent india = new Intent(MenusActivity.this, Detail.class);
                        startActivity(india);
                        break;
                        case 1 :
                            Intent pakistan = new Intent(MenusActivity.this, Detail.class);
                            startActivity(pakistan);
                    }
                }
        });
    }

}