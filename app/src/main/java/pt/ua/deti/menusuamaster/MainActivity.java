package pt.ua.deti.menusuamaster;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "shit";

    Button btBar;
    Button btC;
    Button btS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         btBar = (Button) findViewById(R.id.barbt);
        btBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMesage();
            }
        });
        btC = (Button) findViewById(R.id.crastobt);
        btC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMesage();
            }
        });
        btS = (Button) findViewById(R.id.santiagobt);
        btS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMesage();
            }
        });

    }

    private void sendMesage(){
        Intent intent = new Intent(MainActivity.this, MenusActivity.class);
        startActivity(intent);
    }

}
